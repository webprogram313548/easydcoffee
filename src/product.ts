import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product";
import { Type } from "./entity/Type";

AppDataSource.initialize().then(async () => {

    const typeRepository = AppDataSource.getRepository(Type);
    const drinkType = await typeRepository.findOneBy({ id: 1 });
    const bekeryType = await typeRepository.findOneBy({ id: 2 });
    const foodType = await typeRepository.findOneBy({ id: 3 });

    const productRepository = AppDataSource.getRepository(Product);
    await productRepository.clear();

    var product = new Product();
    product.id = 1;
    product.name = "Americano";
    product.price = 50;
    product.type = drinkType
    await productRepository.save(product);

    product = new Product();
    product.id = 2;
    product.name = "Latte";
    product.price = 60;
    product.type = drinkType
    await productRepository.save(product);

    product = new Product();
    product.id = 3;
    product.name = "Macha";
    product.price = 70;
    product.type = drinkType
    await productRepository.save(product);

    product = new Product();
    product.id = 4;
    product.name = "Carrot Cake";
    product.price = 90;
    product.type = bekeryType
    await productRepository.save(product);

    product = new Product();
    product.id = 5;
    product.name = "Brownei";
    product.price = 60;
    product.type = bekeryType
    await productRepository.save(product);

    product = new Product();
    product.id = 6;
    product.name = "Fried Rice";
    product.price = 50;
    product.type = foodType
    await productRepository.save(product);

    product = new Product();
    product.id = 7;
    product.name = "Sausage";
    product.price = 40;
    product.type = foodType
    await productRepository.save(product);

    const products = await productRepository.find({ 
        relations: { type: true },
    });
    console.log(products);
    
}).catch(error => console.log(error))