import { AppDataSource } from "./data-source"
import { Role } from "./entity/Role";
import { User } from "./entity/User"

AppDataSource.initialize().then(async () => {
    const userRepository = AppDataSource.getRepository(User);
    const rolesRepository = AppDataSource.getRepository(Role);
    
    const adminRole = await rolesRepository.findOneBy({ id: 1 });
    const userRole = await rolesRepository.findOneBy({ id: 2 });

    await userRepository.clear();
    console.log("Inserting a new user into the Memory...");
    var user = new User();
    user.id = 1;
    user.email = "admin@gmail.com";
    user.gender = "male";
    user.password = "Pass@1234";
    user.roles = [];
    user.roles.push(adminRole);
    user.roles.push(userRole);
    console.log("Inserting a new user into the Database...");
    await userRepository.save(user);

    const admin = await userRepository.findOneBy({ id: 1 });
    console.log(admin);

    user = new User();
    user.id = 2;
    user.email = "user1@gmail.com";
    user.gender = "male";
    user.password = "Pass@1234";
    user.roles = [];
    user.roles.push(userRole);
    console.log("Inserting a new user into the Database...");
    await userRepository.save(user);

    const user1 = await userRepository.findOneBy({ id: 2 });
    // const user1 = await userRepository.find({ where: { id: 2, gender: "male" } });
    // const user1 = await userRepository.findOne({ 
    //     where: [{ id: 2 } , { gender: "female" }],
    //  });
    // const user1 = await userRepository.findOne({ 
    //     where: { id: 2, gender: "male" },
    //  });
    console.log(user1);

    user = new User();
    user.id = 3;
    user.email = "user2@gmail.com";
    user.gender = "female";
    user.password = "Pass@1234";
    user.roles = [];
    user.roles.push(userRole);
    console.log("Inserting a new user into the Database...");
    await userRepository.save(user);

    const user2 = await userRepository.findOneBy({ id: 3 });
    console.log(user2);
    
}).catch(error => console.log(error))